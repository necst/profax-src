function Protein() {

    this.aminoacids = [];
    this.angles = [];

    var getTile = function() {
        var molData = '\n OpenBabel04221618553D\n\n  6  5  0  0  0  0  0  0  0  0999 V2000\n    0.0291    0.0000   -0.0000 C   0  7  0  0  0  0  0  0  0  0  0  0\n    1.2526    0.9746   -0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n    1.2526    2.3976   -0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n    2.5149    0.1859   -0.0000 N   0  0  0  0  0  0  0  0  0  0  0  0\n    2.5926   -0.8967    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\n    3.8263    1.0550   -0.0000 C   0  7  0  0  0  0  0  0  0  0  0  0\n  1  2  1  0  0  0  0\n  2  4  1  0  0  0  0\n  3  2  2  0  0  0  0\n  4  6  1  0  0  0  0\n  4  5  1  0  0  0  0\nM  END';
        var metaData = {
            'vectors' : {
                'CANB' : [6,4],
                'CACC' : [1,2],
                'CCNB' : [2,4]
            },
            'Ndel': [],
            'Cdel': []
        };

        return new Aminoacid(molData, metaData);
    };

    var translateChain = function(chain,dx,dy,dz) {
        for(var c in chain) {
            chain[c].translate(dx,dy,dz);
        }
    };

    var rotateChain = function(chain,dx,dy,dz) {
        for(var c in chain) {
            chain[c].rotate(dx,dy,dz);
        }
    };

    var alignTorsionVectorToZ = function(chain, type) {
        var lastItem = chain[chain.length - 1];
        var vector = lastItem.getVector(type);

        var dy = vector[1][1];
        var dz = vector[1][2];

        var angle = Math.atan(dy/dz);
        if(dz < 0) {
            angle = angle + Math.PI;
        }

        rotateChain(chain, angle, 0, 0);
        vector = lastItem.getVector(type);
    };

    var alignChainToVectorOnX = function(chain, type) {
        var lastItem = chain[chain.length - 1];
        var vector = lastItem.getVector(type);
        translateChain(chain, -vector[0][0], -vector[0][1], -vector[0][2]);

        var dx, dy, dz, angle;

        // rotate on Z
        vector = lastItem.getVector(type);
        dx = vector[1][0];
        dy = vector[1][1];

        angle = Math.atan(dy/dx);
        if(dx < 0) {
            angle = angle + Math.PI;
        }
        angle = -angle;

        rotateChain(chain, 0, 0, angle);
        vector = lastItem.getVector(type);

        // rotate on Y
        vector = lastItem.getVector(type);
        dx = vector[1][0];
        dz = vector[1][2];
        angle = 0;

        angle = Math.atan(-dz/dx);
        if(dx < 0) {
            angle = angle + Math.PI;
        }
        angle = -angle;

        rotateChain(chain, 0, angle, 0);
        vector = lastItem.getVector(type);
    };

    var appendToChain = function(chain, item, angle, type) {

        var lastChainItem = chain[chain.length - 1];

        // remove atoms for connections
        lastChainItem.removeAtoms(lastChainItem.getAtomRemoveList(type));
        item.removeAtoms(item.getAtomRemoveList(type));


        // align chain and new item
        alignChainToVectorOnX(chain, type);
        alignChainToVectorOnX([item], type);

        // align vectors to achieve 0 torsion angle
        switch(type) {
            case 'CANB':
                // PHI
                alignTorsionVectorToZ(chain, "CACC");
                alignTorsionVectorToZ([item], "CCNB");
                rotateChain(chain, -angle, 0, 0);
                break;
            case 'CACC':
                // PSI
                alignTorsionVectorToZ(chain, "CCNB");
                alignTorsionVectorToZ([item], "CANB");
                rotateChain([item], -angle, 0, 0);
                break;
        }

        // add item to chain
        chain.push(item);

    };

    var connectAminoacids = function(object) {

        if(object.aminoacids.length <= 1) {
            return object.aminoacids;
        }

        var chain = [];

        // deep copy
        chain.push(jQuery.extend(true, {}, object.aminoacids[0]));

        // start chaining
        for(var i = 1; i < object.aminoacids.length; i++) {
            appendToChain(chain, getTile(), object.angles[i][0], 'CANB');
            appendToChain(chain, jQuery.extend(true, {}, object.aminoacids[i]), object.angles[i][1], 'CACC');
        }

        return chain;
    };

    // **** public methods
    this.appendAminoacid = function(aminoacid, psi, phi) {
        this.aminoacids.push(aminoacid);
        this.angles.push([psi, phi]);
    };

    this.removeAminoacid = function(index) {
        this.aminoacids.splice(index,1);
        this.angles.splice(index,1);
    };

    this.insertAminoacidAt = function(index, aminoacid, psi, phi) {
        this.aminoacids.splice(index, 0, aminoacid);
        this.angles.splice(index, 0, [psi, phi]);
    };

    this.updateAngles = function(index, psi, phi) {
        this.angles[index] = [psi, phi];
    };

    this.getSize = function() {
        return this.aminoacids.length;
    };

    this.getAminoacids = function() {
        // deep copy
        return jQuery.extend(true, [], this.aminoacids);
    };
    this.getAngles = function() {
        // deep copy
        return jQuery.extend(true, [], this.angles);
    };


    this.getMolData = function() {
        var atoms = [];
        var connections = [];
        var offset = 0;

        var chain = connectAminoacids(this);
        for(var am in chain) {
            var aConnections = chain[am].getConnections();
            var aAtoms = chain[am].getAtoms();

            for(var c in aConnections) {
                connections.push([aConnections[c][0] + offset, aConnections[c][1] + offset, aConnections[c][2]]);
            }

            for(var a in aAtoms) {
                atoms.push([aAtoms[a][0], aAtoms[a][1], aAtoms[a][2], aAtoms[a][3]]);
                offset++;
            }
        }

        return Aminoacid.generateMolData(atoms, connections);
    };


}