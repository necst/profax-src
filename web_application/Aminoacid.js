/*
 * Copyright 2016 Lorenzo Di Tucci, Giulia Guidi, Marco Rabozzi
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Freely modified and licensed from http://robotics.stanford.edu/~itayl/mcs/, 
 * with the explicit permission of the autor (Itay Lotan).
*/


function Aminoacid(molData, metaData, shortName) {

    // properties
    this.molData = '';
    this.metaData = '';
    this.atoms = [];
    this.connections = [];
    this.vectors = [];
    this.Cremove = [];
    this.Nremove = [];
    this.shortName = '';

    // **** private methods ****
    var getLineAttr = function(line) {
        return line.trim().split(/[ ]+/);
    };

    var normalizeVector = function(vector) {
        var dx = vector[1][0] - vector[0][0];
        var dy = vector[1][1] - vector[0][1];
        var dz = vector[1][2] - vector[0][2];

        var len = Math.sqrt(dx*dx + dy*dy + dz*dz);
        dx = dx / len;
        dy = dy / len;
        dz = dz / len;

        vector[1][0] = vector[0][0] + dx;
        vector[1][1] = vector[0][1] + dy;
        vector[1][2] = vector[0][2] + dz;

        return vector;
    };

    // constructor
    var init = function(object, molData, metaData, shortName) {
        object.molData = molData;
        object.metaData = metaData;

        var lines = molData.split("\n");
        var attrs = [];

        var fileInfoAttr = getLineAttr(lines[3]);
        var numAtoms = Number(fileInfoAttr[0]);
        var numConnections = Number(fileInfoAttr[1]);

        // get atoms
        for(var i = 4; i < 4 + numAtoms; i++) {
            attrs = getLineAttr(lines[i]);
            attrs[0] = Number(attrs[0]);
            attrs[1] = Number(attrs[1]);
            attrs[2] = Number(attrs[2]);
            object.atoms.push(attrs.splice(0,4));
        }

        // get connections
        for(var j = i; j < i + numConnections; j++) {
            attrs = getLineAttr(lines[j]);
            attrs[0] = Number(attrs[0]);
            attrs[1] = Number(attrs[1]);
            attrs[2] = Number(attrs[2]);
            object.connections.push(attrs.splice(0,3));
        }

        // get meta data
        var a1,a2;
        for(var id in metaData['vectors']) {
            var pair = metaData['vectors'][id];
            a1 = object.atoms[pair[0] - 1];
            a2 = object.atoms[pair[1] - 1];

            object.vectors[id] = normalizeVector([
                [a1[0],a1[1],a1[2]],
                [a2[0],a2[1],a2[2]]
            ]);
        }

        object.Cremove = metaData['Cdel'].sort(function(a,b){return a-b});
        object.Nremove = metaData['Ndel'].sort(function(a,b){return a-b});

        object.shortName = shortName;
    };
    init(this, molData, metaData, shortName);

    var rotatePoint = function(p,dx,dy,dz) {
        var x1,y1,z1;
        x1 = p[0]*Math.cos(dz) - p[1]*Math.sin(dz);
        y1 = p[0]*Math.sin(dz) + p[1]*Math.cos(dz);
        p[0] = x1;
        p[1] = y1;

        y1 = p[1]*Math.cos(dx) - p[2]*Math.sin(dx);
        z1 = p[1]*Math.sin(dx) + p[2]*Math.cos(dx);
        p[1] = y1;
        p[2] = z1;

        x1 = p[0]*Math.cos(dy) + p[2]*Math.sin(dy);
        z1 = -p[0]*Math.sin(dy) + p[2]*Math.cos(dy);
        p[0] = x1;
        p[2] = z1;

        return p;
    };

    var translatePoint = function(p,dx,dy,dz) {
        p[0] += dx;
        p[1] += dy;
        p[2] += dz;
    };


    // **** public methods ****
    this.getMolData = function() {
        return Aminoacid.generateMolData(this.atoms, this.connections);
    };

    this.getAtoms = function() {
        return this.atoms;
    };

    this.getConnections = function() {
        return this.connections;
    };

    this.getShortName = function() {
        return this.shortName;
    }

    this.removeAtoms = function(atomsIndex) {

        if(atomsIndex.length == 0) {
            return;
        }
        atomsIndex = atomsIndex.sort(function(a,b){return a-b});

        // remove atoms
        for (var i = atomsIndex.length - 1; i >= 0; i--) {
            this.atoms.splice(atomsIndex[i] - 1, 1);

            for(var j = this.Cremove.length - 1; j >= 0; j--) {
                if(this.Cremove[j] == atomsIndex[i]) {
                    this.Cremove.splice(j, 1);
                } else if(this.Cremove[j] > atomsIndex[i]) {
                    this.Cremove[j]--;
                }
            }

            for(var j = this.Nremove.length - 1; j >= 0; j--) {
                if(this.Nremove[j] == atomsIndex[i]) {
                    this.Nremove.splice(j, 1);
                } else if(this.Nremove[j] > atomsIndex[i]) {
                    this.Nremove[j]--;
                }
            }
        }

        // remove connections
        for (var i = atomsIndex.length - 1; i >= 0; i--) {
            for (var j = this.connections.length - 1; j >= 0; j--) {
                if (this.connections[j][0] == atomsIndex[i] || this.connections[j][1] == atomsIndex[i]) {
                    this.connections.splice(j, 1);
                }
            }
        }

        // recompute ids for connections
        for (var i = atomsIndex.length - 1; i >= 0; i--) {
            for (var j = this.connections.length - 1; j >= 0; j--) {
                if(this.connections[j][0] >= atomsIndex[i]) {
                    this.connections[j][0]--;
                }
                if(this.connections[j][1] >= atomsIndex[i]) {
                    this.connections[j][1]--;
                }
            }
        }
    };


    this.translate = function(dx,dy,dz) {
        for (var i in this.atoms) {
            translatePoint(this.atoms[i],dx,dy,dz);
        }

        // translate vectors
        for (var i in this.vectors) {
            translatePoint(this.vectors[i][0],dx,dy,dz);
            translatePoint(this.vectors[i][1],dx,dy,dz);
        }

        return this;
    };

    this.rotate = function(dx,dy,dz) {

        for (var i in this.atoms) {
            rotatePoint(this.atoms[i],dx,dy,dz);
        }
        for (var i in this.vectors) {
            rotatePoint(this.vectors[i][0],dx,dy,dz);
            rotatePoint(this.vectors[i][1],dx,dy,dz);
        }

        return this;
    };

    this.getVector = function(type) {
        return this.vectors[type].slice(0);
    };

    this.getAtomRemoveList = function(type) {
        if(type == 'CACC') {
            return this.Cremove.slice(0);
        } else if(type == 'CANB') {
            return this.Nremove.slice(0);
        }
        console.warn('Remove list not found for type: ' + type);
    };
}

Aminoacid.formatNumber = function(number, length, precision) {
    var str = number.toFixed(precision);
    while(str.length < length) {
        str = " " + str;
    }
    return str;
};

Aminoacid.generateMolData = function(atoms, connections) {
    var result = "\n Molecule\n\n";
    result += this.formatNumber(atoms.length,3,0) + this.formatNumber(connections.length,3,0) + "  0  0  0  0  0  0  0 0999 V2000\n";

    for(var a in atoms) {
        result += this.formatNumber(atoms[a][0],10,4) + this.formatNumber(atoms[a][1],10,4) + this.formatNumber(atoms[a][2],10,4) + " " + atoms[a][3] + "\n";
    }

    for(c in connections) {
        result += this.formatNumber(connections[c][0],3,0) + this.formatNumber(connections[c][1],3,0) + this.formatNumber(connections[c][2],3,0) + "\n";
    }

    result += "M END\n";

    return result;
};

Aminoacid.prepareMetaData = function(meta) {
    var lines = meta.split('\n');
    var attrMap = {};

    // parsing
    for(var i in lines) {
        var line = lines[i];
        var attrs = line.split('=');
        var key = attrs[0].trim();
        var val = attrs[1].trim();
        if (key == 'HN' || key == 'HX' || key == 'OC') {
            var items = val.split(',');
            var newVal = [];
            for (var j in items) {
                newVal.push(Number(items[j]));
            }
            val = newVal
        } else {
            val = Number(val);
        }

        attrMap[key] = val;
    }

    return {
        'vectors' : {
            'CANB' : [attrMap['CA'], attrMap['NB']],//CN
            'CACC' : [attrMap['CA'], attrMap['CC']],//CC
            'CCNB' : [attrMap['CC'], attrMap['NB']]
        },
        'Ndel' : [attrMap['HX'][0], attrMap['HX'][1], attrMap['HX'][2], attrMap['NB']],
        'Cdel' : [attrMap['OC'][0], attrMap['OC'][1], attrMap['CC']]
    };
};