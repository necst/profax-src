#!/bin/bash
#export XILINX_OPENCL=/home/user/demo_dac_protein_folding/demo1000iter/pkg/pcie
#export LD_LIBRARY_PATH=$XILINX_OPENCL/runtime/lib/x86_64/lib/
#export XCL_PLATFORM=xilinx_adm-pcie-7v3_1ddr_2_1

source /etc/profile
source /etc/.profile
source /home/user/.bashrc
source /home/user/.bash_profile

./build.exe -I angles.angs
