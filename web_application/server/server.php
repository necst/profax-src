<?php
/*
 * Copyright 2016 Lorenzo Di Tucci, Giulia Guidi, Marco Rabozzi
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Freely modified and licensed from http://robotics.stanford.edu/~itayl/mcs/, 
 * with the explicit permission of the autor (Itay Lotan).
*/



// to start
// go into the gui folder and run:
// php -S localhost:8080

$implementation = $_POST['impl'];
$data = $_POST['data'];

@unlink('output.angs');
@unlink('angles.angs');

file_put_contents('angles.angs', $data);

if($implementation == 'cpu'){
	exec('./mcs -I angles.angs 1> out.txt 2> err.txt');
}else{
	exec("./run.sh 1> out.txt 2> err.txt");
}

if(file_exists('output.angs')) {
	echo file_get_contents('output.angs');
} else {
	echo "ERROR";
}
