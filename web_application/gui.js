/*
 * Copyright 2016 Lorenzo Di Tucci, Giulia Guidi, Marco Rabozzi
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Freely modified and licensed from http://robotics.stanford.edu/~itayl/mcs/, 
 * with the explicit permission of the autor (Itay Lotan).
*/


var proteinCanvas;
var protein = new Protein();
var loadingAnimationStep = 0;
var foldingAnimationStep = 0;
var animationSteps = 100;
var animationTick = 20;
var animationTimer = null;
var generated_button_id = 0;
var psiDefault = Math.PI / 6;
var phiDefault = Math.PI / 6;
var serverAddress = './server/server.php';

$(function(){

    // initialize protein canvas
    proteinCanvas = new ChemDoodle.TransformCanvas3D('protein', $('#protein').width(), 450);
    proteinCanvas.specs.set3DRepresentation('Ball and Stick');
    proteinCanvas.specs.backgroundColor = '#222';
    
    drawAminoButtons();
    updateProtein();

  // loading animation
    setInterval(function(){
        switch(loadingAnimationStep){
            case 0:
                $("#overlayText").html("Folding ..");
                break;
            case 1:
                $("#overlayText").html("Folding. .");
                break;
            case 2:
                $("#overlayText").html("Folding..&nbsp");
                break;
        }
        loadingAnimationStep = (loadingAnimationStep + 1) % 3;
    }, 500);

    setupFoldButtons();
    clearallButton(); 


    /*
     protein = new Protein();

     //create a protein using all the available aminoacids
     var totAcids = 0;
     for(var i = 0; i < 2; i++)
     for(var a in aminodb) {
     var acid = new Aminoacid(aminodb[a]['mol'], Aminoacid.prepareMetaData(aminodb[a]['meta']));

     // initial angles set to 0
     protein.appendAminoacid(acid,0,0);
     totAcids++;
     }

     // draw protein

     transformBallAndStick.loadMolecule(ChemDoodle.readMOL(protein.getMolData(), 1));
     transformBallAndStick.clear();*/
});

function setupFoldButtons() {
    $("#fold_cpu").button();
    $("#fold_cpu").click(function() {
        foldProtein("cpu");
    });
    $("#fold_fpga").button();
    $("#fold_fpga").click(function() {
        foldProtein("fpga");
    });
    $("#revert_protein").button();
    $("#revert_protein").click(function() {
        updateProtein();
    });
}

function drawAminoButtons() {
    var id = 1;
    for(var a in aminodb) {
        var button = '<button id="button_' + id + '" class="btn btn-default btn-sm">' + a + "</button>";
        $(".acids").append(button);

        var jquery_button_id = "#button_" + id;
        console.log(jquery_button_id);
        $(jquery_button_id).button();
        $(jquery_button_id).click(function() {
            addAminoacidButton(this.innerHTML);
            updateProtein();
        });
        id++;
    }
}

function addAminoacidButton(aminoacid) {
    //$(".protein").append('<button name='+aminoacid+' id="generated_button_' + generated_button_id + '" class="btn btn-success btn-sm">' + aminoacid + "</button>");
    $(".protein").append('<button name=' + aminoacid + ' id="generated_button_' + generated_button_id + '" class="btn btn-success btn-sm">' + aminoacid + ' &nbsp<span style="font-size:0.75em;" class="glyphicon glyphicon-remove" aria-label="left align" aria-hidden="true"></span></button>');

    var jquery_button_id = "#generated_button_" + generated_button_id;
    $(jquery_button_id).button();
    $(jquery_button_id).click(function() {
        $(jquery_button_id).remove();
        updateProtein();
    });
    generated_button_id++;

if(generated_button_id>999)
{
window.alert("ERROR: maximum size achieved.");
}   //add a limit size equal to 999;
}

function clearallButton() {
    $("#clear_all").button();
    $("#clear_all").click(function() {
    $(".protein").empty();
    updateProtein();
});
}

function updateProtein() {
    var acidsList = [];

    $(".protein > button").each(function(){
        acidsList.push(this.name);
    });
    
    /*if(acidsList.size>5) {
    // show error overlay
       $("#overlayerror").show();
       $("#overlayText").html("ERROR: maximum size achieved.");
                break;
    }
    */

    if(animationTimer) {
        clearInterval(animationTimer);
        animationTimer = null;
    }

    protein = new Protein();
    for(var a in acidsList) {
        var aminoData = aminodb[acidsList[a]];
        var acid = new Aminoacid(aminodb[acidsList[a]]['mol'], Aminoacid.prepareMetaData(aminoData['meta']), aminoData['input']);
        protein.appendAminoacid(acid,psiDefault,phiDefault);
    }

    drawProtein();
}

function generateInput(){

    var aminoacids = protein.getAminoacids();
    var fileContent = "";
console.log(aminoacids);
    for(var i = 0; i < aminoacids.length; i++) {
        var am = aminoacids[i];
        fileContent = fileContent + am.getShortName() + " " + r2a(psiDefault) + " " + r2a(phiDefault) + " 0\n";
    }

    return fileContent;
}

function parseOutput(data){
    var lines = data.split('\n');
    var angles = [];
    for(var i = 0; i < lines.length; i++) {
        if(lines[i].length > 3) {
            attrs = lines[i].split(' ');
            angles.push([a2r(Number(attrs[1])), a2r(Number(attrs[2]))]) 
        }
    }
    return angles;
}

function drawProtein() {
    /*if(protein.getSize()) {*/
        proteinCanvas.loadMolecule(ChemDoodle.readMOL(protein.getMolData(), 1));
    /*} else {
        proteinCanvas.clear();
    }*/
}

function foldProtein(implementation) {

    // prepare input data for algorithm...
    console.log("generating input");
    var inputData = generateInput();
    console.log("generated input");
    
    // show loading overlay
    $("#overlay").show();
    console.log("show");

    // run algorithm
    switch(implementation) {
        case 'cpu':
            // TODO: ajax call to the server that should call algorithmCallback after success
            $.ajax({
                type: "POST",
                url: serverAddress,
                data: "impl=cpu&" + "data=" + inputData,
                success: function(response){
                    algorithmCallback(response, implementation);
                }   
            });
            break;

        case 'fpga':
            // TODO: ajax call to the server that should call algorithmCallback after success
            $.ajax({
                type: "POST",
                url: serverAddress,
                data: "impl=FPGA&" + "data=" + inputData,
                success: function(response){
                    algorithmCallback(response, implementation);
                }
            });
            break;
    }
}

// $(document).ready(function() {
//     $.ajax({
//         type: "GET",
//         url: "data.txt",
//         dataType: "text",
//         success: function(data) {processData(data);}
//      });
// });

function processData(allText, implementation) {
    var allTextLines = allText.split(/\r\n|\n/);
    // var headers = allTextLines[0].split(',');
    // var lines = [];

    // for (var i=1; i<allTextLines.length; i++) {
    //     var data = allTextLines[i].split(',');
    //     if (data.length == headers.length) {

    //         var tarr = [];
    //         for (var j=0; j<headers.length; j++) {
    //             tarr.push(headers[j]+":"+data[j]);
    //         }
    //         lines.push(tarr);
    //     }
    // }

    // allTextLines[34]; //is what i need
    // var headers = allTextLines[34].split(',');
    // //5 
    // var numOfEnqueues = headers[1];
    // var totTime = headers[2];
    // var minTime = headers[3];
    // var aveTime = headers[4];
    // var maxTime = headers[5];

    var headers = allTextLines[34].split(',');                //5 
    var numOfEnqueues = headers[1];
    var totTime = headers[2];
    var minTime = headers[3];
    var aveTime = headers[4];
    var maxTime = headers[5];

    console.log("numOfEnqueues = "+numOfEnqueues+" totTime = "+totTime+" minTime = "+minTime+" - "+aveTime+" - "+maxTime);
    $('#fpga_time').html(aveTime);

    // console.log("numOfEnqueues = "+numOfEnqueues+" totTime = "+totTime+" minTime = "+minTime+" - "+aveTime+" - "+maxTime);
    // alert(lines);
}

function processDataCpu(allText, implementation) {
    var allTextLines = allText.split(/\r\n|\n/);
    // var headers = allTextLines[0].split(',');
    // var lines = [];

    // for (var i=1; i<allTextLines.length; i++) {
    //     var data = allTextLines[i].split(',');
    //     if (data.length == headers.length) {

    //         var tarr = [];
    //         for (var j=0; j<headers.length; j++) {
    //             tarr.push(headers[j]+":"+data[j]);
    //         }
    //         lines.push(tarr);
    //     }
    // }

    // allTextLines[34]; //is what i need
    // var headers = allTextLines[34].split(',');
    // //5 
    // var numOfEnqueues = headers[1];
    // var totTime = headers[2];
    // var minTime = headers[3];
    // var aveTime = headers[4];
    // var maxTime = headers[5];
    var aveTime = 0;

    for(var i = 0; i < allTextLines.length; i++){
        allTextLines[i] = +allTextLines[i];
    }

    for(var i = 0; i < allTextLines.length; i++){
        aveTime += allTextLines[i];
    }
    aveTime /= allTextLines.length;


    $('#cpu_time').html(aveTime);
    // console.log("numOfEnqueues = "+numOfEnqueues+" totTime = "+totTime+" minTime = "+minTime+" - "+aveTime+" - "+maxTime);
    // alert(lines);
}

function update_table(implementation){
    switch(implementation){
        case 'cpu': 
                    $(document).ready(function() {
                        $.ajax({
                            type: "GET",
                            url: "server/log.csv",
                            dataType: "text",
                            success: function(data) {processDataCpu(data);}
                         });
                    });

                    //$('#cpu_time').html('cpu fringuello pazzesco');
                    break;
        case 'fpga': 
                    $(document).ready(function() {
                        $.ajax({
                            type: "GET",
                            url: "server/sdaccel_profile_summary.csv",
                            dataType: "text",
                            success: function(data) {processData(data);}
                         });
                    });
                    //$('#fpga_time').html('fpga fringuello pazzesco');
                    break;
    }
    

}


function algorithmCallback(response, implementation) {

    console.log("implementation is "+implementation);
    console.log("Server response: " + response);

    // hide loading overlay
    $("#overlay").hide();

    if(response.indexOf('ERROR') > -1) {
        console.log("Algorithm failed to run, check out.err");
        return;
    }

    update_table(implementation);

    var endAngles = parseOutput(response);
    
    // update protein with animation
    var startAngles = protein.getAngles();
    
    // start animation
    foldingAnimationStep = 0;
    animationTimer = setInterval(function() {

        for(var i = 0; i < protein.getSize(); i++) {
            protein.updateAngles(i,
                startAngles[i][0]*(animationSteps - foldingAnimationStep)/animationSteps +
                endAngles[i][0]*foldingAnimationStep/animationSteps,
                startAngles[i][1]*(animationSteps - foldingAnimationStep)/animationSteps +
                endAngles[i][1]*foldingAnimationStep/animationSteps
            );
        }
        drawProtein();
        foldingAnimationStep++;
        if(foldingAnimationStep >= animationSteps) {
            clearInterval(animationTimer);
            animationTimer = null;
        }
    }, animationTick);
}

function getRandomAngles(size) {
    var result = [];
    Math.seedrandom(0);
    for(var i = 0; i < size; i++) {
        result.push([Math.random() * 2 * Math.PI - Math.PI, Math.random() * 2 * Math.PI - Math.PI]);
    }
    return result;
}

function r2a(r) {
    return (r / Math.PI)*180;
}

function a2r(a) {
    return (a / 180)*Math.PI;
}