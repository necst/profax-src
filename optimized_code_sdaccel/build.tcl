
# Define the project for SDAccel
create_solution -name build -dir . -force
add_device xilinx:adm-pcie-7v3:1ddr:2.1

# Host Compiler Flags
set_property -name host_cflags -value "-g -Wall -O3  -D FPGA_DEVICE -D C_KERNEL"  -objects [current_solution]


# Host Source Files
add_files "chain.C"
add_files "crmsd.C"
add_files "eef1.C"
add_files "leaf.C"
add_files "node.C"
add_files "pairtree.C"
add_files "pdb.C"
add_files "rss.C"
add_files "simulation.C"
add_files "slist.C"
add_files "spheres.C"

# Header files
add_files "kernel.h"
add_files "AA.H"
add_files "bv.H"
add_files "chain.H"
add_files "crmsd.H"
add_files "eef1.H"
add_files "leaf.H"
add_files "leafCL.hpp"
add_files "MatVec.h"
add_files "node.H"
add_files "OBB_Disjoint.h"
add_files "pairtree.H"
add_files "pdb.H"
#add_files "RectDist.h"
add_files "RectDistCL.h"
add_files "rotamers.h"
add_files "rss.H"
add_files "slist.H"
add_files "spheres.H"


set_property file_type "c header files" [get_files "kernel.h"]
set_property file_type "c header files" [get_files "AA.H"]
set_property file_type "c header files" [get_files "bv.H"]
set_property file_type "c header files" [get_files "chain.H"]
set_property file_type "c header files" [get_files "crmsd.H"]
set_property file_type "c header files" [get_files "eef1.H"]
set_property file_type "c header files" [get_files "leaf.H"]
set_property file_type "c header files" [get_files "leafCL.hpp"]
set_property file_type "c header files" [get_files "MatVec.h"]
set_property file_type "c header files" [get_files "node.H"]
set_property file_type "c header files" [get_files "OBB_Disjoint.h"]
set_property file_type "c header files" [get_files "pairtree.H"]
set_property file_type "c header files" [get_files "pdb.H"]
set_property file_type "c header files" [get_files "RectDistCL.h"]
set_property file_type "c header files" [get_files "rotamers.h"]
set_property file_type "c header files" [get_files "rss.H"]
set_property file_type "c header files" [get_files "slist.H"]
set_property file_type "c header files" [get_files "spheres.H"]

# Kernel Definition
create_kernel computePairEnergy_k -type c
#set_property max_memory_ports true [get_kernels computePairEnergy]
set_property memory_port_data_width 512 [get_kernels computePairEnergy_k]
add_files -kernel [get_kernels computePairEnergy_k] "kernel.cpp"
set_property -name kernel_flags -value {-g} -objects [get_kernels computePairEnergy_k]


# Define Binary Containers
create_opencl_binary computePairEnergy
set_property region "OCL_REGION_0" [get_opencl_binary computePairEnergy]
create_compute_unit -opencl_binary [get_opencl_binary computePairEnergy] -kernel [get_kernels computePairEnergy_k] -name k1

#report_estimate
# Compile the application to run on the accelerator card
build_system

# Package the application binaries
package_system

